class AddCartRefToOrders < ActiveRecord::Migration
  def change
    add_reference :orders, :cart, index: true
    add_foreign_key :orders, :carts
  end
end
