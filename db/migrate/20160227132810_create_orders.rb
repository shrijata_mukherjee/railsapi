class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.references :user, index: true
      t.references :restaurant, index: true
      t.integer :amount
      t.string :status

      t.timestamps null: false
    end
    add_foreign_key :orders, :users
    add_foreign_key :orders, :restaurants
  end
end
