class OrderSerializer < ActiveModel::Serializer
  attributes :id,
  					 :customer,
  					 :restaurant,
  					 :amount,
  					 :cart
end
