class CartSerializer < ActiveModel::Serializer
  attributes :id,
             :customer,
             :cart_items
end
