class ApplicationController < ActionController::Base
  include DeviseTokenAuth::Concerns::SetUserByToken
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session

  def worker?
  	current_user.try(:worker?)
  end

  def admin?
  	current_user.try(:admin?)
  end

  def customer?
  	current_user.try(:customer?)
  end
end
