class Api::V1::ItemsController < Api::V1::ApplicationController
	
	def index
		serializer_responder(Item.all,each_serializer: ItemSerializer)
	end

	def show
		serializer_responder(Item.find(params[:id]),serializer: ItemSerializer)
	end

end
