class Api::V1::ApplicationController < ApplicationController

  rescue_from ActiveRecord::RecordNotFound do | e |
    handle_api_exception ApiException.new(ApiException.record_not_found)
  end

  rescue_from ::ApiException do | e |
    handle_api_exception(e)
  end

  def default_serializer_options
    {root: false}
  end

  def serializer_responder(resource, config={})
    render json: ResponseBuilder::Main.new(resource, config, params).response
  end

  def handle_api_exception(e)
    render json: ResponseBuilder::Main.new(e, { }, params).response
  end
  
  def unauthorized_access
    render json: {
        error: "You are not authorised"
        },status: 401
  end
  
end
