class Api::V1::Admin::RestaurantController < Api::V1::Admin::ApplicationController
	
	before_action :permit_admin, only: [:update,:destroy] 
	before_action :set_restaurant, only: [:index,:update,:destroy]

	def index
		#render json: current_user
		serializer_responder(@restaurant,serializer: RestaurantSerializer)
	end

	def update
			@restaurant.update(restaurant_params)
			serializer_responder(@restaurant.reload,serializer: RestaurantSerializer)
	end

	def destroy
			serializer_responder(@restaurant.destroy,serializer: RestaurantSerializer)
	end

	private

	def set_restaurant
		@restaurant = current_user.restaurant
	end

	def restaurant_params
		params.require(:restaurant).permit(:name,:location,:opening_hour,:closing_hour)
	end

	def permit_admin
	 	unless admin? 
	 		unauthorized_access
	 	end
	end

end
