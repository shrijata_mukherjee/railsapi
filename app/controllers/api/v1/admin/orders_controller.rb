class Api::V1::Admin::OrdersController <  Api::V1::Admin::ApplicationController
	
	before_action :select_restaurant_orders , only: [:index,:show,:accept,:reject]

	def index
		#render json: current_user
		@orders = current_user.restaurant.pending_orders	
		serializer_responder(@orders, each_serializer: OrderSerializer)	
	end

  def show
  	serializer_responder(@restaurant_orders.find(params[:id]),serializer: OrderSerializer)
  end
  
	def accept
		order = @restaurant_orders.find(params[:id]).accept
	 	serializer_responder(order.reload,serializer: OrderSerializer)
	end

	def reject
		order = @restaurant_orders.find(params[:id]).reject
		serializer_responder(order.reload,serializer: OrderSerializer)		
	end

	private

		def select_restaurant_orders
			@restaurant_orders = current_user.restaurant.orders
		end

end
