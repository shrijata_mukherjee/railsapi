class Api::V1::Admin::ApplicationController < Api::V1::ApplicationController
	#include DeviseTokenAuth::Concerns::SetUserByToken
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  #protect_from_forgery with: :null_session
  
  before_action :authenticate_user!
  before_action :permit_admin_and_worker

  def permit_admin_and_worker
  	if customer?	
  		unauthorized_access
  	end
  end

end
