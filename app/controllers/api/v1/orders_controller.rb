class Api::V1::OrdersController < Api::V1::ApplicationController
	
	before_action :authenticate_user!, only: [:create,:update]
	before_action :check_customer, only: [:create,:update,:index,:show]
	before_action :set_cart, only: :update
	before_action :set_restaurant, only: [:create,:update]
	
	def index
		#render json: current_user
		serializer_responder(current_user.orders,each_serializer: OrderSerializer)
	end

	def show
		serializer_responder(current_user.orders.find(params[:id]),serializer: OrderSerializer)
	end

	def create
		order = current_user.orders.new
		order.restaurant = @restaurant
		order.amount = current_user.active_cart.total
		order.status = "pending"
		order.cart= current_user.active_cart
		order.save
		current_user.carts.create
		serializer_responder(order,serializer: OrderSerializer)
	end

	def update
		 @product = @restaurant.items.find(params[:id])

		 @cart.add(@product, @product.cost.to_f, params[:qty].to_i)

		 serializer_responder(@cart,serializer: CartSerializer)
	end


	private

	 def set_restaurant
	 	@restaurant = Restaurant.find(params[:restaurant_id])
	 end

	 def set_cart
	 	carts = current_user.carts
		@cart = carts.empty? ? current_user.carts.create : current_user.carts.last
	 end

	 def check_customer
	 	unless customer?
	 		unauthorized_access
	 	end
	 end
end
