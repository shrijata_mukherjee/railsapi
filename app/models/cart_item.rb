class CartItem < ActiveRecord::Base
	acts_as_shopping_cart_item_for :cart

	validates :quantity , presence: true ,
											  :numericality => { :only_integer => true,
											  									 :greater_than_or_equal_to => 1 }
end
