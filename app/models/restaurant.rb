class Restaurant < ActiveRecord::Base
 
  has_many :items , dependent: :destroy
  has_many :orders , dependent: :destroy

  has_one :admin
  has_many :workers
	
	validates :location, presence: true, 
	 					:format => { :with => /\A[a-zA-Z ]+\z/  } , :on =>  :update
	validates :opening_hour,:closing_hour , 
	 						:numericality => { :only_integer=>true, :greater_than => 1 , 
	 										  :less_than_or_equal_to => 24 } , :on => :update
  
  delegate :pending_orders , :to => :orders

end
