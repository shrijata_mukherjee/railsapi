class Helpers::User::Search
  attr_accessor :user,
                :results

  def initialize
    @results = ::User.none
  end

  def find(q=nil)
    return if q.blank?
    
    @query = q
    @results = User.where("(lower(name) LIKE ? OR lower(username) LIKE ? OR lower(email) LIKE ?)", "%#{@query}%", "%#{@query}%", "%#{@query}%")
    self
  end
end
