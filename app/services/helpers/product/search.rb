class Helpers::Product::Search
  attr_accessor :results

  def initialize
    @results = ::Product.none
  end

  def find(q='')
    @query = q
    if not @query.blank?
      @query = @query.downcase
      title_match
      url_match
      brand_match
      category_match
      like_match
    end
    self
  end

  def title_match
    @results = ::Product.where('lower(title) = ?', @query).load_assocs
  end

  def url_match
    @results = @results.blank? ? ::Product.where('lower(url) = ?', @query).load_assocs : @results
  end

  def brand_match
    @results = @results.blank? ? ::Product.where('lower(url) = ?', @query).load_assocs : @results
  end

  def category_match
    @results = @results.blank? ? ::Product.includes(:categories).where('categories.name = ?', @query).references(:categories) : @results
  end

  def like_match
    @results = @results.blank? ? ::Product.where("(lower(title) LIKE ? OR lower(manufacturer) LIKE ?)", "%#{@query}%", "%#{@query}%").load_assocs : @results
  end
end
