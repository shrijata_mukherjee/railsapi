class Helpers::Product::Query
  attr_accessor :products

  def initialize(products=::Product.all)
    @products = products.extending(Scopes)
  end

  module Scopes
    def eager_load
      includes(:user, :brand, likes: :liker)
    end

    def liked_by(user)
      where(id: user.likes.pluck(:likeable_id))
    end

    def added_by(user)
      where(user_id: user.id)
    end

    def similar_by_brand_to(product)
      where(brand_id: product.brand_id).where.not(id: product.id)
    end

    def similar_by_category_to(product)
      c = product.categories.last
      return self if !c.present?
      self.belonging_to_category(c).where.not(id: product.id)
    end

    def most_popular
      order(popularity_score: :desc)
    end

    def less_than_amount(a)
      return self if(a.nil? or a.blank?)
      where('(amount/100) <= ?', a.to_f)
    end

    def trending
      order(trending_score: :desc)
    end

    def belonging_to_category(c)
      return self if c.nil?

      categoricals = c.categoricals.where(categorizable_type: 'Product').pluck(:categorizable_id)
      where(id: categoricals)
    end
  end
end
