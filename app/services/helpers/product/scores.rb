class Helpers::Product::Scores
  attr_accessor :product

  def initialize(product)
    @product = product
  end

  def update
    product.popularity_score = Lib::Product.new(product).score('popularity')
    product.trending_score = Lib::Product.new(product).score('trending')
    product.save validate: false
  end
end
