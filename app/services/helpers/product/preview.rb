class Helpers::Product::Preview
  attr_accessor :user

  def initialize(user=nil)
    @user = user
  end

  def from_url(product_url)
    Helpers::Product::Create.new(user).from_url(product_url, false).attributes
  end
end
