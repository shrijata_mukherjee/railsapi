class Helpers::Like::Remove
  attr_accessor :user

  def initialize(user=nil)
    @user = user
  end

  def using_likeable(likeable)
    like = user.unlike(likeable)
    if like.likeable_type.is? 'Product'
      Helpers::Product::Scores.new(likeable).update 
    end
    like
  end
end