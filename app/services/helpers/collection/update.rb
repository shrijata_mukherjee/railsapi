class Helpers::Collection::Update
  attr_accessor :collection

  def initialize(collection=nil)
    @collection = collection
  end

  def with_notification(params)
    was_featured = collection.featured
    collection.maybe.update(params)
    if collection.reload.featured and !was_featured
      FeaturedCollectionNotificationWorker.perform_async(collection.id)
    end
    collection
  end
end
