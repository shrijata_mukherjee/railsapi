class Helpers::View::Create
  attr_accessor :user

  def initialize(user=nil)
    @user = user
  end

  def using_viewable(viewable)
    view = user.view(viewable)
    if view.viewable_type.is? 'Product'
      Helpers::Product::Scores.new(viewable).update 
    end
    view
  end
end