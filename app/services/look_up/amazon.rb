class LookUp::Amazon < LookUp::Base
  attr_accessor :request

  def initialize(item_id)
    super(item_id)
    @request = Vacuum.new('IN')
    @query = { ItemType: 'ASIN' }
    request.configure(
      aws_access_key_id: ENV['AWS_PRODUCT_API_KEY'],
      aws_secret_access_key: ENV['AWS_PRODUCT_API_SECRET'],
      associate_tag: ENV['AWS_ASSOCIATE_TAG']
    )
  end

  # public methods of look up class
  # Every class should implement all these methods

  def large_image_url
    (images['LargeImage']['URL'] || images['ImageSets']['ImageSet']['SmallImage']['URL']) rescue nil
  end

  def medium_image_url
    (images['MediumImage']['URL'] || images['ImageSets']['ImageSet']['MediumImage']['URL']) rescue nil
  end

  def small_image_url
    (images['SmallImage']['URL'] || images['ImageSets']['ImageSet']['SmallImage']['URL']) rescue nil
  end

  def manufacturer
    item_attributes['Manufacturer']
  end

  def product_group
    item_attributes['ProductGroup']
  end

  def title
    item_attributes['Title']
  end

  def currency_code
    offers["OfferSummary"]["LowestNewPrice"]["CurrencyCode"] rescue nil
  end

  def amount
    offers["OfferSummary"]["LowestNewPrice"]["Amount"] rescue nil
  end

  def formatted_price
    offers["OfferSummary"]["LowestNewPrice"]["FormattedPrice"] rescue nil
  end

  def errors
    @errors ||= details['ItemLookupResponse']['Items']['Request']['Errors']['Error'] rescue nil
  end

  def is_valid?
    not errors.present?
  end

  def url
    details["ItemLookupResponse"]["Items"]["Item"]["DetailPageURL"] rescue ''
  end

  # end of public methods

  private

  def find(q)
    request.item_lookup(query: q).to_h
  end

  def offers
    @offers ||= find(@query.merge({ ItemId: item_id, ResponseGroup: 'Offers', MerchantId: 'All'}))['ItemLookupResponse']['Items']['Item'] rescue nil
  end

  def details
    @details ||= find(@query.merge({ ItemId: item_id }))
  end

  def item_attributes
    @item_attributes ||= details['ItemLookupResponse']['Items']['Item']['ItemAttributes'] rescue nil
  end

  def images
    @images ||= find(@query.merge({ ItemId: item_id, ResponseGroup: 'Images' }))['ItemLookupResponse']['Items']['Item'] rescue nil
  end
end
